from django.urls import path
from receipts.views import (receipt_view, create_receipt, category_list
                            ,account_list,create_category, create_account)
from django.shortcuts import redirect






urlpatterns = [
    path("receipts/", receipt_view, name="home"),
    path("receipts/create/", create_receipt, name ="create_receipt"),
    path("receipts/categories/",category_list, name ="category_list"),
    path("receipts/accounts/", account_list, name ="accounts_list"),
    path("receipts/categories/create/",create_category, name="create_category"),
    path("receipts/accounts/create/", create_account, name="create_account")

]
