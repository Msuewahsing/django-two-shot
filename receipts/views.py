from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

@login_required
def receipt_view(request):
    receiptlist = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receiptlist": receiptlist
    }

    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method =="POST":

        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)

            receipt.purchaser = request.user

            receipt.save()

        return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):

    category = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "category_list": category

    }
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        "account_list": accounts
    }

    return render(request, "receipts/account.html", context)
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user

            expense.save()
        return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,

    }
    return render(request, "receipts/categories/create.html",context)
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user

            account.save()
        return redirect("accounts_list")
    else:
        form = AccountForm()
    context = {
        "form": form,

    }
    return render(request, "receipts/accounts/create.html",context)
